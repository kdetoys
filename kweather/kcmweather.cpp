/*
  This file is part of KWeather.
  Copyright (c) 2003 Tobias Koenig <tokoe@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  As a special exception, permission is given to link this program
  with any edition of Qt, and distribute the resulting executable,
  without including the source code for Qt in the source distribution.
*/

#include "kcmweather.h"
#include "dockwidget.h"
#include "serviceinterface.h"

#include <Qt3Support/Q3ButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QLabel>
#include <QtGui/QLayout>
#include <QtGui/QVBoxLayout>
#include <QtGui/QFocusEvent>

#include <kaboutdata.h>
#include <kconfig.h>
#include <kdebug.h>
#include <klocale.h>
#include <kurlrequester.h>
#include <kcombobox.h>
#include <ktoolinvocation.h>
#include <kcolorbutton.h>
#include <kcomponentdata.h>

extern "C"
{
  KDE_EXPORT KCModule *create_weather( QWidget *parent, const char * )
  {
    return new KCMWeather( parent);
  }
}

KCMWeather::KCMWeather( QWidget *parent)
  : KCModule(KComponentData("kweather"))
{
  Q_UNUSED(parent)

  mWeatherService = new OrgKdeKweatherServiceInterface( "org.kde.KWeatherService", "/Service", QDBusConnection::sessionBus() );
  QVBoxLayout *layout = new QVBoxLayout( this );
  mWidget = new prefsDialogData( this );

  mWidget->m_reportLocation->setFocus();
  layout->addWidget( mWidget );
  layout->addStretch();

  fillStationList();
  load();
  connect( mWidget->m_enableLog, SIGNAL( toggled( bool ) ),
                                 SLOT( enableLogWidgets( bool ) ) );
  connect( mWidget->m_viewMode, SIGNAL( released( int ) ),
                                SLOT( changeViewMode( int ) ) );
  connect( mWidget->m_reportLocation, SIGNAL( activated( const QString& ) ),
                                      SLOT( reportLocationChanged() ) );
  connect( mWidget->m_textColor, SIGNAL( changed(const QColor &) ),
                                 SLOT( textColorChanged(const QColor &) ) );

  KAboutData *about = new KAboutData(
      I18N_NOOP( "kcmweather" ), 0,
      ki18n( "KWeather Configure Dialog" ),
      0, KLocalizedString(), KAboutData::License_GPL,
      ki18n( "(c), 2003 Tobias Koenig" ) );

  about->addAuthor( ki18n("Tobias Koenig"), KLocalizedString(), "tokoe@kde.org" );
  setAboutData(about);
}

KCMWeather::~KCMWeather()
{
}
void KCMWeather::showEvent( QShowEvent * )
{
  fillStationList();
}

void KCMWeather::fillStationList()
{
  // store current selection
  QString current = mWidget->m_reportLocation->currentText();

  mWidget->m_reportLocation->clear();

  QStringList stationList = mWeatherService->listStations();
  QStringList::Iterator idx = stationList.begin();

  // get station name from station id for sorting afterwards
  for(; idx != stationList.end(); ++idx)
    *idx = mWeatherService->stationName(*idx);

  stationList.sort();

  idx = stationList.begin();
  for(; idx != stationList.end(); ++idx)
    mWidget->m_reportLocation->insertItem(*idx);

  // restore previous selection
  if ( current.isEmpty() )
  {
    // nothing defined yet; show this situation to the user, otherwise
    // he will see the first available setting which is not what he selected to view
    mWidget->m_reportLocation->insertItem("");
    mWidget->m_reportLocation->setCurrentText("");
  }
  else
  {
    for (int i = 0; i < mWidget->m_reportLocation->count(); i++)
      if (  mWidget->m_reportLocation->text(i) == current )
      {
        mWidget->m_reportLocation->setCurrentIndex(i);
        break;
      }
  }

  if ( current != mWidget->m_reportLocation->currentText() )
    reportLocationChanged();
}

void KCMWeather::changeViewMode( int mode )
{
  mViewMode = mode;
  emit changed( true );
}

void KCMWeather::enableLogWidgets( bool value )
{
  mWidget->m_logFile->setEnabled( value );
  mWidget->m_labelLogFile->setEnabled( value );

  emit changed( true );
}

void KCMWeather::reportLocationChanged()
{
  kDebug() << "New station: " << mWidget->m_reportLocation->currentText()
      << " Code: " << mWeatherService->stationCode( mWidget->m_reportLocation->currentText() ) << endl;
  emit changed( true );
}

void KCMWeather::textColorChanged(const QColor &)
{
  emit changed( true );
}

void KCMWeather::load()
{
  kDebug() << "Load";
  KConfig config( "weather_panelappletrc" );
  KConfigGroup group = config.group("General Options" );
  bool enabled = group.readEntry( "logging", QVariant(false )).toBool();
  mWidget->m_enableLog->setChecked( enabled );
  enableLogWidgets( enabled );

  QColor textColor = group.readEntry("textColor", QColor(Qt::black));
  mWidget->m_textColor->setColor(textColor);

  QString loc = group.readEntry( "report_location" );

  mWidget->m_logFile->setUrl( group.readPathEntry( "log_file_name", QString() ) );

  if ( ! loc.isEmpty() )
    mWidget->m_reportLocation->setCurrentText( mWeatherService->stationName( loc ) );

  mWidget->m_viewMode->setButton( group.readEntry( "smallview_mode", int(dockwidget::ShowAll) ) );
  changeViewMode( group.readEntry( "smallview_mode", int(dockwidget::ShowAll) ) );
  emit changed( false );
}

void KCMWeather::save()
{
  kDebug() << "Save";
  KConfig config( "weather_panelappletrc" );

  KConfigGroup group = config.group("General Options" );
  group.writeEntry( "logging", mWidget->m_enableLog->isChecked() );
  group.writeEntry( "log_file_name", mWidget->m_logFile->url().path() );
  group.writeEntry( "textColor", mWidget->m_textColor->color() );

  // Station idx to local idx; if nothing selected yet, keep it empty
  QString loc;
  if ( ! mWidget->m_reportLocation->currentText().isEmpty() )
    loc = mWeatherService->stationCode( mWidget->m_reportLocation->currentText() );
  group.writeEntry( "report_location", loc);

  group.writeEntry( "smallview_mode", mViewMode );
  group.sync();

  emit changed( false );
}

void KCMWeather::defaults()
{
  mWidget->m_enableLog->setChecked( false );
  enableLogWidgets( false );

  mWidget->m_logFile->setUrl( KUrl("") );
  mWidget->m_reportLocation->setCurrentText( "" );
  changeViewMode( dockwidget::ShowAll );

  emit changed( true );
}

#include "kcmweather.moc"
