/***************************************************************************
 *                                                                         *
 *   Copyright (C) 2002-2003  Ian Reinhart Geiser <geiseri@kde.org>        *
 *   Copyright (C) 2002-2003  Nadeem Hasan <nhasan@kde.org>                *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.         *
 ***************************************************************************/

#include "reportview.h"

#include <stdlib.h>
#include <kcmdlineargs.h>
#include <kdebug.h>
#include <kapplication.h>
#include <kaboutdata.h>
#include <kglobal.h>
#include <klocale.h>
#include <ktoolinvocation.h>

#include <QtDBus/QDBusConnection>
#include <QtDBus/QDBusConnectionInterface>

extern "C" KDE_EXPORT int kdemain(int argc, char *argv[])
{
    KAboutData aboutData("reportview", 0, ki18n("Weather Report"),
        "0.8", ki18n("Weather Report for KWeatherService"),
        KAboutData::License_GPL, ki18n("(C) 2002-2003, Ian Reinhart Geiser"));
    aboutData.addAuthor(ki18n("Ian Reinhart Geiser"), ki18n("Developer"),
        "geiseri@kde.org");
    aboutData.addAuthor(ki18n("Nadeem Hasan"), ki18n("Developer"),
        "nhasan@kde.org");

    KGlobal::locale()->setMainCatalog( "kweather" );

    KCmdLineArgs::init( argc, argv, &aboutData );

    KCmdLineOptions options;
    options.add("+location", ki18n(  "METAR location code for the report" ));
    KCmdLineArgs::addCmdLineOptions( options );
    KCmdLineArgs *args = KCmdLineArgs::parsedArgs();

    if ( args->count() != 1 )
    {
        args->usage();
        return -1;
    }

    KApplication app;

    QString error;
    if(!QDBusConnection::sessionBus().interface()->isServiceRegistered("org.kde.KWeatherService"))
    {
        if (KToolInvocation::startServiceByDesktopName("kweatherservice",
            QStringList(), &error))
        {
            kDebug() << "Starting kweatherservice failed: " << error;
            return -2;
        }
    }

    QString reportLocation = args->arg( 0 );
    args->clear();
    reportView *report = new reportView(reportLocation);

    report->exec();
    
    delete report;

    return 0;
}
