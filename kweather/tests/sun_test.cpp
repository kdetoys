/****************************************************************************
 *              sun_test.cpp  -  Sun Rise and Set Test Program
 *                          -------------------
 *         begin                : Tuesday June 2 2004
 *         copyright            : (C) 2004 - 2006 by John Ratke
 *         email                : jratke@comcast.net
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   This program is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 ****************************************************************************/

#include <QtCore/QString>
#include <QtCore/QDateTime>
#include <QtTest/QtTest>

#include "sun.h"
#include "sun_test.h"

SunTest::SunTest()
   : sun( "42-25N", "087-52W", QDate(2004, 6, 1), -300 )
{
}

void SunTest::testCivil()
{
	QTime civilStart = sun.computeCivilTwilightStart();
	QTime civilEnd   = sun.computeCivilTwilightEnd();

	QCOMPARE( civilStart.hour(),   4 );
	QCOMPARE( civilStart.minute(), 42 );
	QCOMPARE( civilStart.second(), 39 );
	
	QCOMPARE( civilEnd.hour(),   20 );
	QCOMPARE( civilEnd.minute(), 56 );
	QCOMPARE( civilEnd.second(), 6 );
}

void SunTest::testRiseSet()
{
	QTime rise = sun.computeRiseTime();
	QTime set  = sun.computeSetTime();
	
	QCOMPARE( rise.hour(),   5 );
	QCOMPARE( rise.minute(), 16 );
	QCOMPARE( rise.second(), 35 );
	
	QCOMPARE( set.hour(),   20 );     // should be 20
	QCOMPARE( set.minute(), 22 );
	QCOMPARE( set.second(), 10 );
}

QTEST_MAIN(SunTest)
#include "sun_test.moc"

