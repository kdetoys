/****************************************************************************
 *              sun_test.h  -  Sun Rise and Set Test Program
 *                          -------------------
 *         begin                : January 12 2006
 *         copyright            : (C) 2006 by John Ratke
 *         email                : jratke@comcast.net
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   This program is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 ****************************************************************************/

#ifndef SUN_TEST_H
#define SUN_TEST_H

class SunTest : public QObject
{
	Q_OBJECT
public:
	SunTest();

private slots:
	void testCivil();
	void testRiseSet();

private:
	const Sun sun;
};

#endif
