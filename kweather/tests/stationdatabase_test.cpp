/****************************************************************************
 *    stationdatabase_test.cpp  -  Test Program for StationDatabase Class
 *                          -------------------
 *         begin                : Friday June 4 2004
 *         copyright            : (C) 2004 - 2006 by John Ratke
 *         email                : jratke@comcast.net
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   This program is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 ****************************************************************************/

#include <QtCore/QFile>
#include <QtCore/QString>
#include <QtCore/QDateTime>
#include <QtTest/QtTest>

#include "stationdatabase.h"
#include "stationdatabase_test.h"

StationDatabaseTest::StationDatabaseTest()
 : pSDB(NULL)
{
	bool found = false;
	
	// try the stations.dat file in the current directory first.
	QString path("stations.dat");
	
	if (QFile::exists(path))
	{
		found = true;
	}
	else
	{
		// If we couldn't find it, then maybe the build directory is not the
		// same as the source directory.  If that is the case, try to read it
		// from its location in the share/apps/kweatherservice directory, which
		// means that make install would have had to be done first before calling
		// make check so that the file will be there.
	
		QString kdeDirs(getenv("KDEDIR"));
	
		path = kdeDirs + "/share/apps/kweatherservice/stations.dat";
		if (QFile::exists(path))
		{
			found = true;
		}
	}
	
	if (found)
	{
		pSDB = new StationDatabase(path);
	}
}

void StationDatabaseTest::testStationNameLoading_data()
{
	QTest::addColumn<QString>("locationCode");
	QTest::addColumn<QString>("locationName");

	QTest::newRow("initial")        << "KORD" << "Chicago, Chicago-O'Hare International Airport";
	QTest::newRow("unknown")        << "KXYZ" << "Unknown Station";
	QTest::newRow("already loaded") << "KORD" << "Chicago, Chicago-O'Hare International Airport";
}

void StationDatabaseTest::testStationNameLoading()
{
	QFETCH(QString, locationCode);
	QFETCH(QString, locationName);

	if ( pSDB != NULL )
	{
		QCOMPARE(pSDB->stationNameFromID(locationCode), locationName);
	}
	else
	{
		QSKIP("This test requires that the station database be able to load its data", SkipAll);
	}
}

void StationDatabaseTest::testLatitudeFromID_data()
{
	QTest::addColumn<QString>("locationCode");
	QTest::addColumn<QString>("latitude");

	QTest::newRow("initial")        << "KPWK" << "42-07-15N";
	QTest::newRow("unknown")        << "KZZZ" << "Unknown Station";
	QTest::newRow("already loaded") << "KPWK" << "42-07-15N";
}

void StationDatabaseTest::testLatitudeFromID()
{
	QFETCH(QString, locationCode);
	QFETCH(QString, latitude);

	QCOMPARE(pSDB->stationLatitudeFromID(locationCode), latitude);
}

void StationDatabaseTest::testLongitudeFromID_data()
{
	QTest::addColumn<QString>("locationCode");
	QTest::addColumn<QString>("longitude");

	QTest::newRow("initial")        << "KAAA" << "089-20-06W";
	QTest::newRow("unknown")        << "KZYZ" << "Unknown Station";
	QTest::newRow("already loaded") << "KAAA" << "089-20-06W";
}

void StationDatabaseTest::testLongitudeFromID()
{
	QFETCH(QString, locationCode);
	QFETCH(QString, longitude);

	QCOMPARE(pSDB->stationLongitudeFromID(locationCode), longitude);
}

void StationDatabaseTest::testCountryFromID_data()
{
	QTest::addColumn<QString>("locationCode");
	QTest::addColumn<QString>("country");

	QTest::newRow("initial")        << "KUGN" << "United States";
	QTest::newRow("unknown")        << "KYYY" << "Unknown Station";
	QTest::newRow("already loaded") << "KUGN" << "United States";
}

void StationDatabaseTest::testCountryFromID()
{
	QFETCH(QString, locationCode);
	QFETCH(QString, country);

	QCOMPARE(pSDB->stationCountryFromID(locationCode), country);
}


QTEST_MAIN(StationDatabaseTest)
#include "stationdatabase_test.moc"

