/****************************************************************************
 *           metar_parser_test.cpp  -  MetarParser Test Program
 *
 *         Run all of the unit tests with "make check". Run just this 
 *         test with "make metar_parser_test; ./metar_parser_test"
 *                          -------------------
 *         begin                : Wednesday June 16 2004
 *         copyright            : (C) 2004 - 2006 by John Ratke
 *         email                : jratke@comcast.net
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   This program is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 ****************************************************************************/

#include <QtCore/QDateTime>
#include <QtCore/QFile>
#include <QtCore/QStringList>
#include <QtTest/QtTest>

#include "stationdatabase.h"
#include "metar_parser.h"
#include "metar_parser_test.h"

const int localUTCOffset = -300;

StationDatabase *MetarParserTest::stationDb = NULL;

void MetarParserTest::getStationDb()
{
	bool found = false;
	
	// try the stations.dat file in the current directory first.
	QString path("stations.dat");
	
	if (QFile::exists(path))
	{
		found = true;
	}
	else
	{
		// If we couldn't find it, then maybe the build directory is not the
		// same as the source directory.  If that is the case, try to read it
		// from its location in the share/apps/kweatherservice directory, which
		// means that make install would have had to be done first before calling
		// make check so that the file will be there.
	
		QString kdeDirs(getenv("KDEDIR"));
	
		path = kdeDirs + "/share/apps/kweatherservice/stations.dat";
		if (QFile::exists(path))
		{
			found = true;
		}
	}
	
	if (found)
	{
		stationDb = new StationDatabase(path);
	}

}

void MetarParserTest::testParsing_data()
{
	QTest::addColumn<int>("year");
	QTest::addColumn<int>("month");
	QTest::addColumn<int>("day");
	QTest::addColumn<int>("hour");
	QTest::addColumn<int>("minute");
	QTest::addColumn<QString>("station");
	QTest::addColumn<QString>("metarstr");

	QTest::addColumn<QString>("weather");
	QTest::addColumn<QString>("cover");
	QTest::addColumn<QString>("vis");
	QTest::addColumn<QString>("pres");
	QTest::addColumn<QString>("temp");
	QTest::addColumn<QString>("dew");
	QTest::addColumn<QString>("relhu");
	QTest::addColumn<QString>("wind");
	QTest::addColumn<QString>("windchill");
	QTest::addColumn<QString>("heatind");
	QTest::addColumn<QString>("winddir");
	QTest::addColumn<QString>("reportloc");
	QTest::addColumn<bool>("maint");

	QTest::newRow("1")  << 2004 << 6 << 17 << 21 << 7 << 
	                      "KUGN" << "2004/06/18 00:55 KUGN 180055Z AUTO 04004KT 9SM SCT050 17/15 A3005 RMK AO2 SLP167 T01670150 TSNO" <<
			      "cloudy2_night" << "Scattered clouds at 5000 feet" << 
			      "9m" << "30.05\" Hg" << "62.1�F" << "59�F" << "89.7%" << "4 MPH" << 
			      QString() << QString() << "NE" << "KUGN" << false;
			      
	QTest::newRow("2")  << 2004 << 6 << 18 << 18 << 43 << 
	                      "KUGN" << "2004/06/18 22:55 KUGN 182255Z 29005KT 10SM BKN110 21/13 A3010 RMK AO2 SLP187 T02110133" <<
			      "cloudy3" << "Broken clouds at 11000 feet" << 
			      "10m" << "30.10\" Hg" << "70�F" << "55.9�F" << "61.1%" << "5 MPH" << 
			      QString() << QString() << "WNW" << "KUGN" << false;

}

void MetarParserTest::testParsing()
{
	QFETCH(int, year);
	QFETCH(int, month);
	QFETCH(int, day);
	QFETCH(int, hour);
	QFETCH(int, minute);

	QDate date(year, month, day);
	QTime time(hour, minute);

	// Construct a MetarParser object for our tests.
	if ( stationDb == NULL )
	{
		getStationDb();
		if ( stationDb == NULL )
		{
			QFAIL("No station database");
		}
	}
	MetarParser parser( stationDb, KLocale::Imperial, date, time, localUTCOffset );

	QFETCH(QString, station);
	QFETCH(QString, metarstr);

	struct WeatherInfo results = parser.processData(station, metarstr);

	QFETCH(QString, weather);
	QFETCH(QString, cover);
	QFETCH(QString, vis);
	QFETCH(QString, pres);
	QFETCH(QString, temp);
	QFETCH(QString, dew);
	QFETCH(QString, relhu);
	QFETCH(QString, wind);
	QFETCH(QString, windchill);
	QFETCH(QString, heatind);
	QFETCH(QString, winddir);
	QFETCH(QString, reportloc);
	QFETCH(bool, maint);

	QCOMPARE(results.theWeather,      weather);
	QCOMPARE(results.qsCoverList[0],  cover);
	QCOMPARE(results.qsVisibility,    vis);
	QCOMPARE(results.qsPressure,      pres);
	QCOMPARE(results.qsTemperature,   temp);
	QCOMPARE(results.qsDewPoint,      dew);
	QCOMPARE(results.qsRelHumidity,   relhu);
	QCOMPARE(results.qsWindSpeed, 	  wind);
	QCOMPARE(results.qsWindChill,     windchill);
	QCOMPARE(results.qsHeatIndex,     heatind);
	QCOMPARE(results.qsWindDirection, winddir);
	QCOMPARE(results.reportLocation,  reportloc);
	QCOMPARE(results.stationNeedsMaintenance,
	                                  maint);
}


QTEST_MAIN(MetarParserTest)
#include "metar_parser_test.moc"

