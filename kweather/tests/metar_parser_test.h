/****************************************************************************
 *           metar_parser_test.h  -  Metar Parsing Test Program
 *                          -------------------
 *         begin                : January 18 2006
 *         copyright            : (C) 2006 by John Ratke
 *         email                : jratke@comcast.net
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   This program is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 ****************************************************************************/

#ifndef METAR_PARSER_TEST_H
#define METAR_PARSER_TEST_H

class MetarParserTest : public QObject
{
	Q_OBJECT

private slots:
	void testParsing_data();
	void testParsing();

private:
	void getStationDb();
	static StationDatabase *stationDb;
	
};

#endif
