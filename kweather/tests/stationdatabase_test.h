/****************************************************************************
 *    stationdatabase_test.h  -  Test Program for StationDatabase Class
 *                          -------------------
 *         begin                : January 12 2006
 *         copyright            : (C) 2006 by John Ratke
 *         email                : jratke@comcast.net
 ****************************************************************************/

/****************************************************************************
 *                                                                          *
 *   This program is free software; you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation; either version 2 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 ****************************************************************************/

#ifndef STATIONDATABASE_TEST_H
#define STATIONDATABASE_TEST_H

class StationDatabaseTest : public QObject
{
	Q_OBJECT
public:
	StationDatabaseTest();

private slots:
	void testStationNameLoading();
	void testStationNameLoading_data();
	void testLatitudeFromID();
	void testLatitudeFromID_data();
	void testLongitudeFromID();
	void testLongitudeFromID_data();
	void testCountryFromID();
	void testCountryFromID_data();

private:
	StationDatabase *pSDB;
};	

#endif
