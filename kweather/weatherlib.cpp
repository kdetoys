/***************************************************************************
			weatherlib.cpp  -  description
			-------------------
begin                : Wed Jul 5 2000
copyright            : (C) 2000 by Ian Reinhart Geiser
email                : geiseri@msoe.edu
***************************************************************************/

/***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************/

#include "weatherlib.h"
#include "metar_parser.h"
#include "stationdatabase.h"
#include "sun.h"

#include <QtCore/QFile>
#include <QtCore/QDateTime>
#include <QtCore/QTextStream>

#include <math.h>
#include <unistd.h>

#include <kglobal.h>
#include <klocale.h>
#include <kdebug.h>
#include <kdatetime.h>
#include <kio/job.h>
#include <kurl.h>
#include <ktemporaryfile.h>
#include <kpassivepopup.h>

#include "weatherlib.moc"

class WeatherLib::Data
{
	public:
		Data();
		~Data(){ if ( target ) delete target; }

		void clear();

		/** The current weather state outside */
		struct WeatherInfo wi;
		QDateTime age;
		KTemporaryFile *target;
		bool downloading;
		bool updated;
		KIO::Job *job;
};

WeatherLib::Data::Data()
		: target( 0 ), job( 0 )
{
	clear();
}

void WeatherLib::Data::clear()
{
	age = QDateTime::currentDateTime();
	downloading = false;
	updated = false;
	job = 0;

	delete target;
	target = 0;
}

WeatherLib::WeatherLib(StationDatabase *stationDB, QObject *parent, const char *name)
	: QObject (parent, name)
{
	KGlobal::locale()->insertCatalog("kweather");

	m_StationDb    = stationDB;

	data.setAutoDelete( true );
}

WeatherLib::~WeatherLib()
{
}

void WeatherLib::slotCopyDone(KJob* job)
{
	kDebug(12006) << "Copy done...";
	if( job->error())
	{
		kDebug(12006) << "Error code: " << job->error();
		//job->showErrorDialog(0L);
		if(job->error() == KIO::ERR_COULD_NOT_CONNECT ||
			job->error() == KIO::ERR_UNKNOWN_HOST)
		{
			hostDown= true;
			// no need to show a passive popup here, as below all stations will show "dunno" icon
		}
	}
	// Find the job
	Q3DictIterator<Data> it( data );
	Data *d = 0L;
	for( ; it.current(); ++it )
	{
		kDebug(12006) << "Searching for job...";
		if(it.current()->job == job)
		{
			d = it.current();
			d->downloading = false;
			if( !job->error() )
			{
				kDebug( 12006) << "Reading: " << d->target->fileName();
				QTextStream *t = new QTextStream( d->target );
				if( t )
				{
					QString s;
					while ( !t->atEnd() )
					{
						s += ' ' + t->readLine();
					}

					if ( !s.isEmpty() )
					{
						kDebug( 12006 ) << "Parse: " << s;
						MetarParser parser(m_StationDb, KGlobal::locale()->measureSystem());
						d->wi = parser.processData(d->wi.reportLocation, s);
						d->age = QDateTime::currentDateTime().addSecs(1800);
						emit fileUpdate(d->wi.reportLocation);
						d->updated = true;
					}
					else
					{
						// File error
						kDebug( 12006 ) << "File empty error...";
						KPassivePopup::message( i18n("KWeather Error"),
						i18n("The temp file %1 was empty.", d->target->fileName()), (QWidget*)NULL);
						d->updated = false;
					}
				}
				else
				{
					// File error
					kDebug( 12006 ) << "File read error...";
					KPassivePopup::message( i18n("KWeather Error"),
				i18n("Could not read the temp file %1.", d->target->fileName()), (QWidget*)NULL);
					d->updated = false;
				}
				delete d->target;
				d->target = 0L;
				d->job = 0L;

			}
			else if( job->error()  == KIO::ERR_DOES_NOT_EXIST)
			{
				data.remove(d->wi.reportLocation);
				kDebug( 12006 ) << "Bad station data so i am going to remove it";
				KPassivePopup::message( i18n("KWeather Error"),
				i18n("The requested station does not exist."),  (QWidget*)NULL );
			}
			else if(job->error() == KIO::ERR_COULD_NOT_CONNECT ||
				job->error() == KIO::ERR_UNKNOWN_HOST)
			{
				kDebug( 12006 ) << "Offline now...";
				d->clear();
				d->wi.theWeather = "dunno";
				d->wi.qsCurrentList.append(i18n("The network is currently offline..."));
				d->wi.qsCurrentList.append(i18n("Please update later."));
				emit fileUpdate(d->wi.reportLocation);
			}
			else
			{
				kDebug( 12006 ) << "Duh?...";
			}

		}
	}
}

void WeatherLib::getData(Data *d)
{
	if(!d->downloading && !hostDown)
	{
		d->downloading = true;
		d->updated = false;
		QString u = "http://weather.noaa.gov/pub/data/observations/metar/stations/";
		u += d->wi.reportLocation.toUpper().trimmed();
		u += ".TXT";

		d->target = new KTemporaryFile();
		d->target->setSuffix("-weather");
		d->target->open();

		KUrl url(u);
		KUrl local;
                local.setPath(d->target->fileName());

		d->job = KIO::file_copy( url, local, -1, KIO::Overwrite | KIO::HideProgressInfo);
		d->job->addMetaData("cache", "reload"); // Make sure to get fresh info
		connect( d->job, SIGNAL( result( KJob *)),
			SLOT(slotCopyDone(KJob *)));
		kDebug( 12006 ) << "Copying " << url.prettyUrl() << " to "
			<< local.prettyUrl() << endl;
		emit fileUpdating(d->wi.reportLocation);
	}
}

WeatherLib::Data* WeatherLib::findData(const QString &stationID)
{
	Data *d = data[stationID];
	if (!d)
	{
		d = new Data();
		d->wi.reportLocation = stationID;
		d->wi.theWeather = "dunno";
		d->wi.qsCurrentList.append( i18n( "Retrieving weather data..." ) );
		data.insert(stationID, d);
		getData(d);
	}

	return d;
}

QString WeatherLib::temperature(const QString &stationID){
	Data *d = findData(stationID);
	return d->wi.qsTemperature;
}

QString WeatherLib::pressure(const QString &stationID){
	Data *d = findData(stationID);
	return d->wi.qsPressure;
}

QString WeatherLib::wind(const QString &stationID){
	Data *d = findData(stationID);
	return (d->wi.qsWindSpeed + ' ' + d->wi.qsWindDirection);
}

/**  */
QString WeatherLib::dewPoint(const QString &stationID){
	Data *d = findData(stationID);
	return d->wi.qsDewPoint;
}

QString WeatherLib::relHumidity(const QString &stationID){
	Data *d = findData(stationID);
	return d->wi.qsRelHumidity;
}

QString WeatherLib::heatIndex(const QString &stationID){
	Data *d = findData(stationID);
	return d->wi.qsHeatIndex;
}

QString WeatherLib::windChill(const QString &stationID){
	Data *d = findData(stationID);
	return d->wi.qsWindChill;
}

QString WeatherLib::iconName(const QString &stationID){

	QString result("dunno");

	// isEmpty is true for null or 0 length strings
	if ( !stationID.isEmpty() )
	{
		Data *d = findData(stationID);
		result = d->wi.theWeather;
	}

	return result;
}

QString WeatherLib::date(const QString &stationID){
	Data *d = findData(stationID);

  if ( ! d->wi.qsDate.isValid() )
    return "";
  else
  {
    KDateTime gmtDateTime(d->wi.qsDate, d->wi.qsTime, KDateTime::UTC);
    KDateTime localDateTime = gmtDateTime.toTimeSpec(KDateTime::LocalZone);
    return KGlobal::locale()->formatDateTime(localDateTime, KLocale::LongDate);
  }
}

/** Returns the current cover */
QStringList WeatherLib::cover(const QString &stationID){
	Data *d = findData(stationID);
	return d->wi.qsCoverList;
}

/** return the visibility */
QString WeatherLib::visibility(const QString &stationID){
	Data *d = findData(stationID);
	return d->wi.qsVisibility;
}

/** return the weather text */
QStringList WeatherLib::weather(const QString &stationID){
	Data *d = findData(stationID);
	return d->wi.qsCurrentList;
}

bool WeatherLib::stationNeedsMaintenance(const QString &stationID)
{
	Data *d = findData(stationID);
	return d->wi.stationNeedsMaintenance;
}

void WeatherLib::update(const QString &stationID)
{
	// refresh hostdown just to see
	hostDown = false;
	// Only grab new data if its more than 50 minutes old
	Data *d = findData(stationID);

	QDateTime timeout = QDateTime::currentDateTime();

	kDebug (12006) << "Current Time: " << KGlobal::locale()->formatDateTime(timeout, KLocale::LongDate, false) <<
			" Update at: " << KGlobal::locale()->formatDateTime(d->age, KLocale::LongDate, false) << endl;
	if( timeout >= d->age || !d->updated)
		getData(d);
	else
		emit fileUpdate(d->wi.reportLocation);
}

QStringList WeatherLib::stations()
{
	QStringList l;
	Q3DictIterator<Data> it( data );
	for( ; it.current(); ++it )
		l += it.currentKey();
	return l;
}

void WeatherLib::forceUpdate(const QString &stationID)
{
	Data *d = findData(stationID);

	getData( d );
}

void WeatherLib::remove(const QString &stationID)
{
	data.remove(stationID);
	emit stationRemoved(stationID);
}
