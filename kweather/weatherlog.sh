#!/bin/sh

LOC=KEWR
TEMP=`qdbus org.kde.KWeatherService /Service temperature $LOC`
WIND=`qdbus org.kde.KWeatherService /Service wind $LOC`
VIS=`qdbus org.kde.KWeatherService /Service visibility $LOC`
DATE=`date`
echo "$DATE,$TEMP,$WIND,$VIS" >> $LOC.log
