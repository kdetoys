//
//
// C++ Interface: $MODULE$
//
// Description:
//
//
// Copyright 2003 Ian Reinhart Geiser <geiseri@yahoo.com>
//
// GNU General Public License version 2
// See COPYING file that comes with this distribution
//
//
#ifndef STATIONDATABASE_H
#define STATIONDATABASE_H

#include <QtCore/QMap>
#include <QtCore/QString>

#include <klocale.h>
#include <kstandarddirs.h>

/**
This is the main database for mapping METAR codes to Station information.

@author ian reinhart geiser
*/

class StationInfo
{
    public:
        QString cityName;
        QString country;
        QString longitude;
        QString latitude;
        StationInfo () {}
};
class StationDatabase
{
public:
    StationDatabase(const QString &path = KStandardDirs::locate("data", "kweatherservice/stations.dat"));
    ~StationDatabase();

    QString stationNameFromID(const QString& id);

    QString stationLongitudeFromID( const QString &stationID);

    QString stationLatitudeFromID(const QString &stationID);

    QString stationCountryFromID( const QString &stationID);
    
    QString stationIDfromName( const QString &name );

private:
    QMap<QString, StationInfo> theDB;

    bool loadStation( const QString & stationID );

    const QString mPath;
};

#endif // STATIONDATABASE_H
