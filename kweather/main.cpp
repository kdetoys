// Copyright 2003 Ian Reinhart Geiser <geiseri@yahoo.com>
//
// GNU General Public License version 2
// See COPYING file that comes with this distribution

#include <kuniqueapplication.h>
#include <kcmdlineargs.h>
#include <kdebug.h>
#include <kaboutdata.h>
#include <klocale.h>
#include "weatherservice.h"
int main (int argc, char *argv[])
{
	KAboutData aboutdata("KWeatherService", "kdelibs4", ki18n("KDE"),
				"0.8", ki18n("KWeather D-Bus Service"),
				KAboutData::License_GPL, ki18n("(C) 2002, Ian Reinhart Geiser"));
	aboutdata.addAuthor(ki18n("Ian Reinhart Geiser"),ki18n("Developer"),"geiseri@kde.org");
	aboutdata.addAuthor(ki18n("Nadeem Hasan"),ki18n("Developer"),"nhasan@kde.org");
        aboutdata.addCredit(ki18n("Laurent Montel"), ki18n("Conversion to D-Bus"),"montel@kde.org");


	KCmdLineArgs::init( argc, argv, &aboutdata );
	// KCmdLineArgs::addCmdLineOptions( options );
	KUniqueApplication::addCmdLineOptions();

	if (!KUniqueApplication::start())
	{
		kDebug() << "dbusservice is already running!";
		return (0);
	}

	KUniqueApplication app;
	// This app is started automatically, no need for session management
	app.disableSessionManagement();
	new WeatherService(&app, "WeatherService");
	return app.exec();

}
