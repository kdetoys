//
//
// C++ Implementation: $MODULE$
//
// Description:
//
//
// Copyright 2003 Ian Reinhart Geiser <geiseri@yahoo.com>
//
// GNU General Public License version 2
// See COPYING file that comes with this distribution
//
//

#include "stationdatabase.h"

#include <QtCore/QStringList>
#include <QtCore/QFile>
#include <QtCore/QTextStream>

#include <kdebug.h>

#if 0
class StationInfo
{
	public:
		QString cityName;
		QString country;
		QString longitude;
		QString latitude;
		StationInfo () {}
};
#endif

StationDatabase::StationDatabase(const QString &path) : mPath(path)
{
}


StationDatabase::~StationDatabase()
{
}

bool StationDatabase::loadStation( const QString & stationID )
{
	QFile file( mPath );
	bool found = false;

	if ( !file.open( QIODevice::ReadOnly ) )
		return false;

	QTextStream stream( &file );
	stream.setEncoding( QTextStream::UnicodeUTF8 );
	QString line;
	while ( !stream.atEnd() )
	{
		line = stream.readLine(); // line of text excluding '\n'
		QStringList data = line.split( ';', QString::KeepEmptyParts);

		if ( data[ 0 ].trimmed() == stationID )
		{
			StationInfo station;
			station.cityName = data[ 3 ].trimmed();
			station.country = data[ 5 ].trimmed();
			station.latitude = data[ 7 ].trimmed();
			station.longitude = data[ 8 ].trimmed();

			theDB.insert( data[ 0 ], station );
			found = true;
			break;
		}
	}

	file.close();
	return found;
}

/*!
    \fn StationDatabase::stationNameFromID(const QString& id)
 */
QString StationDatabase::stationNameFromID( const QString & stationID )
{
	QString result;
	
	if ( theDB.find( stationID ) == theDB.end() )
	{
		if ( loadStation( stationID ) )
			result = theDB[ stationID ].cityName;
		else
			result = i18n( "Unknown Station" );
	}
	else
	{
		result = theDB[ stationID ].cityName;
	}

	return result;
}

/*!
    \fn StationDatabase::stationLongitudeFromID( const QString &stationID)
 */
QString StationDatabase::stationLongitudeFromID( const QString & stationID )
{
	QString result;
	
	if ( theDB.find( stationID ) == theDB.end() )
	{
		if ( loadStation( stationID ) )
			result = theDB[ stationID ].longitude;
		else
			result = i18n( "Unknown Station" );
	}
	else
	{
		result = theDB[ stationID ].longitude;
	}

	return result;
}

/*!
    \fn StationDatabase::stationLatitudeFromID(const QString &stationID)
 */
QString StationDatabase::stationLatitudeFromID( const QString & stationID )
{
	QString result;
	
	if ( theDB.find( stationID ) == theDB.end() )
	{
		if ( loadStation( stationID ) )
			result = theDB[ stationID ].latitude;
		else
			result = i18n( "Unknown Station" );
	}
	else
	{
		result = theDB[ stationID ].latitude;
	}

	return result;
}

/*!
    \fn StationDatabase::stationCountryFromID( const QString &stationID)
 */
QString StationDatabase::stationCountryFromID( const QString &stationID )
{
	QString result;
	
	if ( theDB.find( stationID ) == theDB.end() )
	{
		if ( loadStation( stationID ) )
			result = theDB[ stationID ].country;
		else
			result = i18n( "Unknown Station" );
	}
	else
	{
		result = theDB[ stationID ].country;
	}

	return result;
}

QString StationDatabase::stationIDfromName( const QString &name )
{
	QMap<QString,StationInfo>::Iterator itr = theDB.begin();
	for( ; itr != theDB.end(); ++itr)
	{
	  kDebug() << "Checking " << itr.data().cityName;
	  if( itr.data().cityName == name )
		return itr.key();
	}
	return "0000";
}
