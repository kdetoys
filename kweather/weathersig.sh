#!/bin/sh

LOC=KPTW
TEMP=`qdbus org.kde.KWeatherService /Service temperature $LOC`
WIND=`qdbus org.kde.KWeatherService /Service wind $LOC`
DATE=`qdbus org.kde.KWeatherService /Service date $LOC`
VIS=`qdbus org.kde.KWeatherService /Service visibility $LOC`
NAME=`qdbus org.kde.KWeatherService /Service stationName $LOC`
EMAIL="geiseri@kde.org"
echo "===+<KWeather for KDE>+=+<http://www.kde.org>+==="
echo "   Report for $NAME" 
echo "		on $DATE"
echo "   $TEMP with winds at $WIND and $VIS of visibility."
echo "===============================+<$EMAIL>+==="
